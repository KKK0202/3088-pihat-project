# 3088 PiHat Project

A pi UPS hat, which helps power the Raspberry pi if there are power cuts, has LED as indicators of how much battery is left in the Pi and also shuts down the Pi safely when the better gets below critical level.

The plan is to have this Project on a public scale so everyone can contribute/use it. Initially have the normally powering device module that comes standard with the Raspberry Pi would not be touched but would rather be the same powering module that is also charging the UPS battery and poweringthe Pi at the same time. 
The battery and the actual power input would be through a comparator to check if the power from the socket is lower/higher than the battery, if it is higher it would charge the battery and if lower the battery would power the pi.
